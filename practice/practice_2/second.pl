/*
 * The "-->" syntax makes the input list and output list arguments implicit.
 * The body consists of terminals and non-terminals
 * We can use { } to include Prolog logic (goals) in the middle of the grammar.
 * The `phrase` predicate is equivalent to 'wff'.
 */

% To treat "" as a list of chars for better readability
:- set_prolog_flag(double_quotes, chars).
:- (discontiguous wff/1).

% Definitive Clause Grammars for Decimals Formal Language
digit -->
    "0"| "1"| "2"| "3"| "4"| "5"| "6"| "7"| "8"| "9".
number -->
    digit| digit, number.
nonzero -->
    "1"| "2"| "3"| "4"| "5"| "6"| "7"| "8"| "9".
left -->
    "0"| nonzero| nonzero, number.
right -->
    nonzero| number, nonzero.
decimal -->
    left| left, ".", right.

wff(X) :-
    phrase(decimal, X).

stars -->
    "*"| "*", stars.
sentence -->
    stars,
    "d",
    stars,
    "c",
    stars.

wff(X) :-
    phrase(sentence, X).

emoji -->
    eyes, mouth| eyes, nose, mouth.
eyes -->
    ":".
nose -->
    "−"| "-", nose.
mouth -->
    "("| ")".

wff(X) :-
    phrase(emoji, X).

% Axiom A
theorem(":(") :-
    format('Axiom A: :( ~n').

% Rule 1.
theorem(LongerNose) :-
    append([Prefix, "-", Mouth], LongerNose),
    phrase(mouth, Mouth),
    append([Prefix, Mouth], ShorterNose),
    theorem(ShorterNose),
    format('Rule 1: ~s --> ~s ~n', [ShorterNose, LongerNose]).

% Rule 2.
theorem(HappyFace) :-
    append([Prefix, ")"], HappyFace),
    append([Prefix, "("], SadFace),
    theorem(SadFace),
    format('Rule 2: ~s --> ~s ~n', [SadFace, HappyFace]).
