from tokens import TokenType
from nodes import *


class Parser:
    def __init__(self, tokens):
        self.tokens = iter(tokens)
        self.advance()

    def advance(self):
        try:
            self.current_token = next(self.tokens)
        except StopIteration:
            self.current_token = None

    def parse(self):
        if self.current_token == None:
            return None

        result = self.expr()

        if self.current_token != None:
            raise Exception("Unparsed tokens left")

        return result

    def expr(self):
        result = self.term()

        token_types = [TokenType.PLUS, TokenType.MINUS]

        while self.current_token != None and self.current_token.type in token_types:
            if self.current_token.type == TokenType.PLUS:
                self.advance()
                result = AddNode(result, self.term())
            elif self.current_token.type == TokenType.MINUS:
                self.advance()
                result = SubtractNode(result, self.term())

        return result

    def term(self):
        result = self.factor()

        token_types = [TokenType.MULTIPLY, TokenType.DIVIDE]

        while self.current_token != None and self.current_token.type in token_types:
            if self.current_token.type == TokenType.MULTIPLY:
                self.advance()
                result = MultiplyNode(result, self.factor())
            elif self.current_token.type == TokenType.DIVIDE:
                self.advance()
                result = DivideNode(result, self.factor())

        return result

    def factor(self):

        token = self.current_token

        if self.current_token.type == TokenType.LPAREN:
            self.advance()
            result = self.expr()

            if self.current_token.type != TokenType.RPAREN:
                raise Exception('Invalid parentheses')

            self.advance()
            return result

        if self.current_token.type == TokenType.NUMBER:
            self.advance()
            return NumberNode(token.value)

        raise Exception(f"Invalid factor token: {self.current_token}")
