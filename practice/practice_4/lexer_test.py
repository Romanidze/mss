import unittest

from tokens import Token, TokenType
from lexer import Lexer


class TestLexer(unittest.TestCase):
    def test_empty(self):
        tokens = list(Lexer("").generate_tokens())
        self.assertEqual(tokens, [], "failed on empty string")

    def test_one_number(self):
        tokens = list(Lexer("5").generate_tokens())
        self.assertEqual(tokens, [Token(TokenType.NUMBER, 5)],
                         "failed on empty spaces string")

    def test_spaces_with_number(self):
        tokens = list(Lexer(" \t\t\t\t\n\n\n   \n5").generate_tokens())
        self.assertEqual(tokens, [Token(TokenType.NUMBER, 5)],
                         "failed on empty spaces string")
