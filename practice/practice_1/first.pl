male(james1).
male(charles1).
male(charles2).
male(james2).
male(george1).

female(catherine).
female(elizabeth).
female(sophia).

parent(james1, charles1).
parent(james1, elizabeth).
parent(charles1, catherine).
parent(charles1, charles2).
parent(charles1, james2).
parent(elizabeth, sophia).
parent(sophia, george1).

% head :- body

mother(M, X) :- 
    parent(M, X),
    female(M).

father(F, X) :-
    parent(F, X),
    male(F).

sibling(X, Y) :-
    parent(Z, X),
    parent(Z, Y),
    X \= Y.

sister(S, X) :-
    sibling(S, X),
    female(S).

aunt(Aunt, N) :-
    parent(Z, N),
    sibling(Aunt, Z),
    female(Aunt).

grandparent(GP, GC) :-
    parent(Z, GC),
    parent(GP, Z).

ancestor(Ancestor, Descendant) :-
    parent(Ancestor, Descendant).

ancestor(Ancestor, Descendant) :-
    parent(Ancestor, Z),
    ancestor(Z, Descendant).

size([], 0).

size([Head|Tail], Length) :-
    size(Tail, TailLength), Length is TailLength+1.
