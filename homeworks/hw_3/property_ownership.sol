pragma solidity 0.4.26;
pragma experimental ABIEncoderV2;

/**
 * @title Contract for the controlling of property ownership process
 * @dev definition of contract with all required classes (structs) and methods
 */
contract PropertyOwnership {
    
    /**
     * @dev struct for holding the Person definition
     * @field personAddress particular address of a person
     * @field isBuyer flag for checking, if this person a buyer or not
     * @field isSeller flag for checking, if this person a seller or not
     */ 
    struct Person {
        address personAddress;
        bool isBuyer;
        bool isSeller;
    }
    
    // enum for holding the time period, when each ownership period will be hold 
    enum TimePeriod {
        Year, Month, Day
    }
    
    // definition of action type to be hold
    enum ActionType {
        Buy, Sell
    }
    
    /**
     * @dev struct for holding the Ownership Period information
     * @field startDate start date of ownership period
     * @field endDate end date of ownership period
     * @field durationPeriod how long will the period be hold
     */
    struct OwnershipPeriod {
        uint256 startDate;
        uint256 endDate;
        TimePeriod durationPeriod;
    }
    
    // defintion of property name to be represent
    struct PropertyInfo {
        string propertyName;
    }
    
    /**
     * @dev struct for holding the ownership request information
     * @field person information for the person, which created the request
     * @field selectedProperty property, for which person created a request
     * @field actionType what action will be done - buy or sell
     * @field ownershipPeriod for how long person want to hold the property
     */ 
    struct RequestInfo {
        Person person;
        PropertyInfo selectedProperty;
        ActionType actionType;
        OwnershipPeriod ownershipPeriod;
    }
    
    //definitino of all the linksberween person and all request types
    mapping(address => Person) personBuyMappings;
    mapping(address => Person) personSellMappings;
    
    mapping(address => RequestInfo[]) buyActions;
    mapping(address => RequestInfo[]) sellActions;
    
    modifier datesCheck(uint256 startDate, uint256 endDate) {
        require(startDate < endDate);
        _;
    }
    
    function buyProperty(address sellerAddress, OwnershipPeriod ownershipPeriod, PropertyInfo propertyInfo) public payable datesCheck(ownershipPeriod.startDate, ownershipPeriod.endDate) {
        
        personBuyMappings[address(this)] = Person(address(this), true, false);
        personSellMappings[sellerAddress] = Person(sellerAddress, false, true);
        
        buyActions[address(this)].push(
            RequestInfo(Person(address(this), true, false), propertyInfo, ActionType.Buy, ownershipPeriod)
        );
        
        sellActions[sellerAddress].push(
            RequestInfo(Person(sellerAddress, false, true), propertyInfo, ActionType.Sell, ownershipPeriod)
        );
        
        
    }
    
    function sellProperty(address buyerAddress, OwnershipPeriod ownershipPeriod, PropertyInfo propertyInfo) public payable datesCheck(ownershipPeriod.startDate, ownershipPeriod.endDate) {
        
        personBuyMappings[buyerAddress] = Person(buyerAddress, true, false);
        personSellMappings[address(this)] = Person(address(this), false, true);
        
        buyActions[buyerAddress].push(
            RequestInfo(Person(buyerAddress, true, false), propertyInfo, ActionType.Buy, ownershipPeriod)
        );
        
        sellActions[address(this)].push(
            RequestInfo(Person(address(this), false, true), propertyInfo, ActionType.Sell, ownershipPeriod)
        );
        
    }
    
}