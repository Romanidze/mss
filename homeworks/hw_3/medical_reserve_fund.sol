pragma solidity 0.4.26;

/**
 * @title Contract for the medical reserve fund
 * @dev definition of contract with all required classes (structs) and methods
 */
contract MedicalReserveFund {
    
    /**
     * @dev struct for holding the Person definition
     * @field moneyAmount definition of money amount, which Person has
     * @field userAddress direct link to the person
     * @field flag, which checks , if the person is in mapping or not
     */
    struct Person {
        uint moneyAmount;
        address userAddress;
        bool isExist;
    }
    
    /**
     * @dev struct for holding the Expenditure Request definition
     * @field moneyAmount moeny amount to be transferred
     * @field medkitName medkit, for which money are being transferred
     */
    struct ExpenditureRequest {
        uint moneyAmount;
        string medkitName;
    }
    
    /**
     * @dev struct for holding all votes to be processed
     * @field votesInfo array with 0 / 1 int values. 0 is a representation for a "No" vote, 1 - for a "Yes" vote
     */ 
    struct VotesEntity{
        int[] votesInfo;
    }
    
    uint private limitValue = 10 ether;
    
    /**
     * @dev Basic mappings between all data structs
     */ 
    mapping (address => Person) participants;
    mapping (address => VotesEntity) votesForParticipant;
    mapping (address => ExpenditureRequest) requestsOfPerson;
    
    //modifier for checking if hareholder pass the limit or not
    modifier shareholderLimitCheck() {
        
        Person searchablePerson = participants[msg.sender];
        
        if(searchablePerson.userAddress == address(0) || !searchablePerson.isExist){
            
            require(msg.value < limitValue);
            participants[msg.sender] = Person(msg.value, msg.sender, true);
            
        }else{
            
            require(searchablePerson.moneyAmount < limitValue);
            participants[msg.sender] = Person(searchablePerson.moneyAmount + msg.value, msg.sender, true);
            
        }
        
        _;
        
    }
    
    /**
     * @dev method for performing request to fund transferring
     * @param moneyAmount amount of moeny to be transferred
     * @param medkitName name of medkit, for which money are being transferred
     */ 
    function makeRequest(uint moneyAmount, string medkitName) public payable shareholderLimitCheck {
        requestsOfPerson[address(this)] = ExpenditureRequest(moneyAmount, medkitName);
    }
    
    /**
     * @dev method for peroforming votes for the requests pf participants, which transfer money bigger than limitValue
     * @param participantAddress address of participant to vote
     * @param voteType type of vote to be set. 0 is a representation for a "No" vote, 1 - for a "Yes" vote
     */
    function makeVote(address participantAddress, int voteType) public payable {
        
        VotesEntity votesEntity = votesForParticipant[participantAddress];
        
        require(voteType == 0 || voteType == 1);
        
        votesEntity.votesInfo.push(voteType);
        votesForParticipant[participantAddress] = votesEntity;
        
    }
    
    /**
     * @dev method of getting the voting results for particular participants
     * @param participantAddress address of particular participant
     * @return result of voting for the participant request
     */
    function getVotingResult(address participantAddress) public returns (string) {
        
        VotesEntity votesEntity = votesForParticipant[participantAddress];
        
        int256[] votes = votesEntity.votesInfo;
        
        uint votesCount = votes.length;
        
        uint256 trueCount = 0;
        uint256 falseCount = 0;
        
        for (uint i = 0; i < votesCount; i++){
            
            int256 elem = votes[i];
            
            if(elem == 0){
                trueCount++;
            }else{
                falseCount++;
            }
            
        }
        
        if(trueCount == falseCount){
            return "DRAW";
        }
        
        if(trueCount > falseCount && trueCount > votesCount / 2){
            return "ACCEPT";
        }else if(falseCount > trueCount && falseCount > votesCount / 2){
            return "DENY";
        }else{
            return "DRAW";
        }
        
    }
    
}