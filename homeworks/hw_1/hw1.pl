:- set_prolog_flag(double_quotes, chars).
:- (discontiguous is_stars/1).
:- (discontiguous is_emoji/1).

% 1. Stars, Incompleteness, Derivation
stars -->
    "*"| "*", stars.
sum_sentence -->
    stars,
    "diamond",
    stars,
    "circle",
    stars.

multiply_sentence -->
    stars,
    "square",
    stars,
    "circle",
    stars.

is_stars_sum(X) :-
    phrase(sum_sentence, X).

is_stars_multiply(X) :-
    phrase(multiply_sentence, X).

%-------------------------------------------------------------------------------------------

% 2. Emojis
eyes -->
    ":".
nose -->
    "−"| "-", nose.
mouth -->
    "("| ")".
emoji -->
    eyes, mouth| eyes, nose, mouth.

is_emoji(X) :-
    phrase(emoji, X).

% Axiom A
theorem(":(") :-
    format('Axiom A: :( ~n').

% Rule 1.
theorem(LongerNose) :-
    append([Prefix, "-", Mouth], LongerNose),
    phrase(mouth, Mouth),
    append([Prefix, Mouth], ShorterNose),
    theorem(ShorterNose),
    format('Rule 1: ~s --> ~s ~n', [ShorterNose, LongerNose]).

% Rule 2.
theorem(HappyFace) :-
    append([Prefix, ")"], HappyFace),
    append([Prefix, "("], SadFace),
    theorem(SadFace),
    format('Rule 2: ~s --> ~s ~n', [SadFace, HappyFace]).

% derivation of :(|- :- --)
theorem(VeryLongNose) :-
    theorem(SadFace),
    append([Prefix, "|-"], VeryLongNose),
    append([Prefix, Nose], LongerNose),
    phrase(nose, Nose),
    append([Prefix, Nose], LongerNose),
    append([Prefix, ")"], HappyFace).

%-------------------------------------------------------------------------------------------

% 3. Basic Algebraic Expression Grammar

parser([]) --> [].
parser(Tree) --> assign(Tree).

assign(assignment(ident(X), '=', Exp)) --> id(X), [=], expr(Exp), [;].

id(X) --> [X], { atom(X) }.

expr(expression(Term)) --> term(Term).
expr(expression(Term, Op, Exp)) --> term(Term), add_sub(Op), expr(Exp).

term(term(F)) --> factor(F).
term(term(F, Op, Term)) --> factor(F), mul_div(Op), term(Term).

factor(factor(int(N))) --> num(N).
factor(factor(Exp)) --> ['('], expr(Exp), [')'].

add_sub(Op) --> [Op], { memberchk(Op, ['+', '-']) }.
mul_div(Op) --> [Op], { memberchk(Op, ['*', '/']) }.

num(N) --> [N], { number(N) }.

% Usage: phrase(parser(T), [a, =, 3, +, '(', 6, *, 11, ')', ;]).
