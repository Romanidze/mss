import unittest
from nodes import *
from interpreter import Interpreter
from values import Number

class TestInterpreter(unittest.TestCase):

  def test_empty(self):
    with self.assertRaises(Exception):
      value = Interpreter().visit(None)
    
  def test_number(self):
    value = Interpreter().visit(NumberNode(21.2))

    self.assertEqual(value,Number(21.2))

  def test_expression(self):
    # 5 - 3/3 * 10.0
    value = Interpreter().visit(
      SubtractNode(
        NumberNode(5),
        MultiplyNode(
          DivideNode(
            NumberNode(3),
            NumberNode(3)
          ),
          NumberNode(10)
        )
      )
    )

    self.assertEqual(value, Number(-5))

  def test_smaller(self):
    value = Interpreter().visit(SmallCheckNode(NumberNode(30.0), NumberNode(50.0)))
    self.assertTrue(value)

  def test_smaller_equal(self):
    value = Interpreter().visit(SmallOrEqualCheckNode(NumberNode(30.0), NumberNode(30.0)))
    self.assertTrue(value)

  def test_equal(self):
    value = Interpreter().visit(EqualCheckNode(NumberNode(30.0), NumberNode(30.0)))
    self.assertTrue(value)

  def test_bigger(self):
    value = Interpreter().visit(BiggerCheckNode(NumberNode(50.0), NumberNode(30.0)))
    self.assertTrue(value)

  def test_bigger_equal(self):
    value = Interpreter().visit(BiggerCheckNode(NumberNode(50.0), NumberNode(50.0)))
    self.assertTrue(value)
