import unittest
from tokens import Token, TokenType
from parser_ import Parser
from nodes import *

class TestParser(unittest.TestCase):

  def test_empty(self):
    tokens = []
    tree = Parser(tokens).parse()
    self.assertEqual(tree, None)

  def test_numbers(self):
    tokens = [Token(TokenType.NUMBER, 51.2)]
    tree = Parser(tokens).parse()
    self.assertEqual(tree, NumberNode(51.2))

  def test_individual_operations(self):
    tokens = [
      Token(TokenType.NUMBER, 27),
      Token(TokenType.PLUS),
      Token(TokenType.NUMBER, 14),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, AddNode(NumberNode(27), NumberNode(14)))

    tokens = [
      Token(TokenType.NUMBER, 27),
      Token(TokenType.MINUS),
      Token(TokenType.NUMBER, 14),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, SubtractNode(NumberNode(27), NumberNode(14)))

    tokens = [
      Token(TokenType.NUMBER, 27),
      Token(TokenType.MULTIPLY),
      Token(TokenType.NUMBER, 14),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, MultiplyNode(NumberNode(27), NumberNode(14)))

    tokens = [
      Token(TokenType.NUMBER, 27),
      Token(TokenType.DIVIDE),
      Token(TokenType.NUMBER, 14),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, DivideNode(NumberNode(27), NumberNode(14)))

    tokens = [
      Token(TokenType.NUMBER, 27),
      Token(TokenType.SMALLER),
      Token(TokenType.NUMBER, 14),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, SmallCheckNode(NumberNode(27), NumberNode(14)))

    tokens = [
      Token(TokenType.NUMBER, 35),
      Token(TokenType.SMALLER_EQUAL),
      Token(TokenType.NUMBER, 21),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, SmallOrEqualCheckNode(NumberNode(35), NumberNode(21)))

    tokens = [
      Token(TokenType.NUMBER, 10),
      Token(TokenType.EQUAL),
      Token(TokenType.NUMBER, 10),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, EqualCheckNode(NumberNode(10), NumberNode(10)))

    tokens = [
      Token(TokenType.NUMBER, 5),
      Token(TokenType.BIGGER),
      Token(TokenType.NUMBER, 7),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, BiggerCheckNode(NumberNode(5), NumberNode(7)))

    tokens = [
      Token(TokenType.NUMBER, 5),
      Token(TokenType.BIGGER_EQUAL),
      Token(TokenType.NUMBER, 7),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, BiggerOrEqualCheckNode(NumberNode(5), NumberNode(7)))

  def test_full_expression(self):
    tokens = [
      # 27 + (43 / 36 - 48) * 51.2
      Token(TokenType.NUMBER, 27),
      Token(TokenType.PLUS),
      Token(TokenType.LPAREN),
      Token(TokenType.NUMBER, 43),
      Token(TokenType.DIVIDE),
      Token(TokenType.NUMBER, 36),
      Token(TokenType.MINUS),
      Token(TokenType.NUMBER, 48),
      Token(TokenType.RPAREN),
      Token(TokenType.MULTIPLY),
      Token(TokenType.NUMBER, 51),
    ]

    tree = Parser(tokens).parse()
    self.assertEqual(tree, AddNode(
      NumberNode(27),
      MultiplyNode(
        SubtractNode(
          DivideNode(
            NumberNode(43),
            NumberNode(36)
          ),
          NumberNode(48)
        ),
        NumberNode(51)
      )
    ))
