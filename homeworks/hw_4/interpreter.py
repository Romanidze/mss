from nodes import *
from values import Number, Boolean

class Interpreter:

  def visit(self, node):

    if not node: 
      raise Exception('No tree passed to interpreter')

    method_name = f'visit_{type(node).__name__}'
    method = getattr(self, method_name)
    return method(node)

  def visit_NumberNode(self, node):
    return Number(node.value)
  
  def visit_AddNode(self, node):
    return Number(self.visit(node.node_a).value + self.visit(node.node_b).value)

  def visit_SubtractNode(self, node):
    return Number(self.visit(node.node_a).value - self.visit(node.node_b).value)

  def visit_MultiplyNode(self, node):
    return Number(self.visit(node.node_a).value * self.visit(node.node_b).value)
  
  def visit_DivideNode(self, node):
    return Number(self.visit(node.node_a).value / self.visit(node.node_b).value)

  def visit_SmallCheckNode(self, node):
    return Boolean(self.visit(node.node_a).value < self.visit(node.node_b).value)

  def visit_SmallOrEqualCheckNode(self, node):
    return Boolean(self.visit(node.node_a).value <= self.visit(node.node_b).value)
  
  def visit_EqualCheckNode(self, node):
    return Boolean(self.visit(node.node_a).value == self.visit(node.node_b).value)

  def visit_BiggerCheckNode(self, node):
    return Boolean(self.visit(node.node_a).value > self.visit(node.node_b).value)

  def visit_BiggerOrEqualCheckNode(self, node):
    return Boolean(self.visit(node.node_a).value >= self.visit(node.node_b).value)
