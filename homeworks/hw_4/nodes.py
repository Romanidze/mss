from dataclasses import dataclass


@dataclass
class NumberNode:
  value: float
  
  def __repr__(self):
    return f"{self.value}"
  
@dataclass
class AddNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a}+{self.node_b})"

@dataclass
class SubtractNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a}-{self.node_b})"

@dataclass
class MultiplyNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a}*{self.node_b})"


@dataclass
class DivideNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a}/{self.node_b})"

@dataclass
class SmallCheckNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a} < {self.node_b})"

@dataclass
class SmallOrEqualCheckNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a} =< {self.node_b})"

@dataclass
class EqualCheckNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a} == {self.node_b})"

@dataclass
class BiggerCheckNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a} > {self.node_b})"

@dataclass
class BiggerOrEqualCheckNode:
  node_a: any
  node_b: any

  def __repr__(self):
    return f"({self.node_a} >= {self.node_b})"