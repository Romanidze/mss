import unittest
from tokens import Token, TokenType
from lexer import Lexer

class TestLexer(unittest.TestCase):

  def test_empty(self):
    tokens = list(Lexer("").generate_tokens())
    self.assertEqual(tokens, [], 'Failed on empty string')

  def test_empty_spaces(self):
    tokens = list(Lexer(" \t\t\t\n\n\n   \n").generate_tokens())
    self.assertEqual(tokens, [], 'Failed on empty spaces string')

  def test_one_number(self):
    tokens = list(Lexer("5").generate_tokens())
    self.assertEqual(tokens, [Token(TokenType.NUMBER, 5)], 'Failed on one number string')

  def test_spaces_with_number(self):
    tokens = list(Lexer(" \t\t\t \n\n\n   \n5").generate_tokens())
    self.assertEqual(tokens, [Token(TokenType.NUMBER, 5)], 'Failed on  spaces string')

  def test_numbers(self):
    tokens = list(Lexer("123 123.456 123. .456 .").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 123.000),
      Token(TokenType.NUMBER, 123.456),
      Token(TokenType.NUMBER, 123.000),
      Token(TokenType.NUMBER, 000.456),
      Token(TokenType.NUMBER, 000.000),
    ])

  def test_operators(self):
    tokens = list(Lexer("+-*/").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.PLUS),
      Token(TokenType.MINUS),
      Token(TokenType.MULTIPLY),
      Token(TokenType.DIVIDE),
    ])

  def test_parens(self):
    tokens = list(Lexer("()").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.LPAREN),
      Token(TokenType.RPAREN),
    ])

  def test_all(self):
    tokens = list(Lexer("27 + (43 / 36 - 48) * 51.3").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 27.0),
      Token(TokenType.PLUS),
      Token(TokenType.LPAREN),
      Token(TokenType.NUMBER, 43.0),
      Token(TokenType.DIVIDE),
      Token(TokenType.NUMBER, 36.0),
      Token(TokenType.MINUS),
      Token(TokenType.NUMBER, 48.0),
      Token(TokenType.RPAREN),
      Token(TokenType.MULTIPLY),
      Token(TokenType.NUMBER, 51.3),
    ])

  def test_smaller(self):
    tokens = list(Lexer("27 < 51.3").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 27.0),
      Token(TokenType.SMALLER),
      Token(TokenType.NUMBER, 51.3),
    ])

  def test_smaller_or_equal(self):
    tokens = list(Lexer("27 =< 51.3").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 27.0),
      Token(TokenType.SMALLER_EQUAL),
      Token(TokenType.NUMBER, 51.3),
    ])

  def test_equal(self):
    tokens = list(Lexer("10 == 10").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 10.0),
      Token(TokenType.EQUAL),
      Token(TokenType.NUMBER, 10.0),
    ])
  
  def test_bigger(self):
    tokens = list(Lexer("30 > 10").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 30.0),
      Token(TokenType.BIGGER),
      Token(TokenType.NUMBER, 10.0),
    ])

  def test_bigger_or_equal(self):
    tokens = list(Lexer("30 >= 10").generate_tokens())
    self.assertEqual(tokens, [
      Token(TokenType.NUMBER, 30.0),
      Token(TokenType.BIGGER_EQUAL),
      Token(TokenType.NUMBER, 10.0),
    ])