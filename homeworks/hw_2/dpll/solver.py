
input_file = open('input.txt', 'r')

sentence = input_file.readline()
input_file.close()

def parse_expr(input_str):

    disjunctions_str = input_str.split(' and ')
    disjunctions = []

    for elem in disjunctions_str:

        # remove brackets 
        elem = elem[1:-1]

        literals_str = elem.split(' or ')
        literals = set()

        for elem1 in literals_str:
            is_negative = False
            if elem1.startswith('not '):
                is_negative = True

                # skipping the not and space
                elem1 = elem1[4:]
            literals.add((elem1, is_negative))

        disjunctions.append(literals)
    
    return disjunctions

# get only literal letter
def select_literal(cnf):
    for elem in cnf:
        for elem1 in elem:
            return elem1[0]

# recursive search alghorithm
#
# pick arbitrary literal, set to true
#
#  all conjuncts containing the literal drop out, and all conjuncts containing its negation have its negation removed
#
# if result not retrieved, set that literal to false and repeat procedure
def dpll(cnf, assignments = {}):

    if len(cnf) == 0:
        return True, assignments

    if any([len(c)==0 for c in cnf]):
        return False, None

    l = select_literal(cnf)

    new_cnf = [c for c in cnf if (l, True) not in c]
    new_cnf = [c.difference({(l, False)}) for c in new_cnf]
    sat, vals = dpll(new_cnf, {**assignments, **{l: True}})
    if sat:
        return sat, vals

    new_cnf = [c for c in cnf if (l, False) not in c]
    new_cnf = [c.difference({(l, True)}) for c in new_cnf]
    sat, vals = dpll(new_cnf, {**assignments, **{l: False}})
    if sat:
        return sat, vals

    return False, None

input_expr = parse_expr(sentence)
result, _ = dpll(input_expr)

output_file = open('output.txt', 'w')

if(result):
    output_file.write('SAT')
else:
    output_file.write('UNSAT')

output_file.close()
